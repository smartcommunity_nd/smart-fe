module App exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Http
import API.Login as Login exposing (..)


-- MAIN


main : Program Never Model Msg
main =
    program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { identity : String
    , password : String
    , isRunning : Bool
    , data : LoginResponse
    }



-- type alias LoginResult =
--     { resultCode : Int -- 0 = sucess
--     , data : List a
--     }


init : ( Model, Cmd Msg )
init =
    ( Model "" "" False (Login.LoginResponse 0 Nothing), Cmd.none )



-- MESSAGES


type Msg
    = NoOp
    | SetPassword String
    | SetIdentity String
    | DoLogin
    | LoginDone (Result Http.Error LoginResponse)



-- | UpdateCanLogin Bool
-- VIEW


canILogin : Model -> Bool
canILogin model =
    (not (String.isEmpty model.password)) && (not (String.isEmpty model.identity))


runningMessage : Model -> String
runningMessage model =
    if model.isRunning then
        "Running..."
    else
        ""


view : Model -> Html Msg
view model =
    div []
        [ input [ type_ "text", placeholder "Indentity...", onInput SetIdentity ] []
        , input [ type_ "password", placeholder "Password...", onInput SetPassword ] []
        , button [ disabled (not (canILogin model) || model.isRunning), onClick DoLogin ] [ text "Login" ]
        , div [] [ text (runningMessage model) ]
        ]



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        SetPassword newPassword ->
            ( { model | password = newPassword }, Cmd.none )

        SetIdentity newId ->
            ( { model | identity = newId }, Cmd.none )

        DoLogin ->
            ( { model | isRunning = True }, Http.send LoginDone (login model) )

        LoginDone result ->
            case result of
                Ok response ->
                    ( { model | data = response, isRunning = False }, Cmd.none )

                Err _ ->
                    let
                        data =
                            -- System error
                            Login.LoginResponse 999 Nothing
                    in
                        ( { model | data = data, isRunning = False }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
