module API.Login exposing (LoginResponse, login)

import Http
import HttpBuilder exposing (RequestBuilder, withExpect, withQueryParams)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (decode, required)
import Json.Encode as Encode exposing (Value)
import API.Helpers exposing (apiUrl)


type alias LoginResponse =
    { resultCode : Int
    , data : Maybe String
    }


loginResponseDecoder : Decoder LoginResponse
loginResponseDecoder =
    decode LoginResponse
        |> required "resultCode" Decode.int
        |> required "data" (Decode.nullable Decode.string)


login : { r | identity : String, password : String } -> Http.Request LoginResponse
login { identity, password } =
    let
        data =
            Encode.object
                [ ( "identity", Encode.string identity )
                , ( "password", Encode.string password )
                ]

        body =
            Encode.object
                [ ( "data", data ) ]
                |> Http.jsonBody
    in
        loginResponseDecoder |> Http.post (apiUrl "/login") body
